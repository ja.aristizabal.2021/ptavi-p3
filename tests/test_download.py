#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import unittest
from unittest.mock import patch, call

import download

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

calls_urlretrieve = [
    call('http://www.content-networking.com/smil/hello.jpg', 'fichero-0.jpg'),
    call('http://www.content-networking.com/smil/earthrise.jpg', 'fichero-1.jpg'),
    call('http://www.content-networking.com/smil/hello.wav', 'fichero-2.wav'),
    call('http://gsyc.es/~grex/letra.rt', 'fichero-3.rt')
]



class TestDownload(unittest.TestCase):

    def setUp(self):
        os.chdir(parent_dir)

    @patch.object(download, 'urlretrieve')
    def test_download(self, test_patch):
        test_patch.return_value = True
        download.main('karaoke.smil')
        test_patch.assert_has_calls(calls_urlretrieve)


if __name__ == '__main__':
    unittest.main()
